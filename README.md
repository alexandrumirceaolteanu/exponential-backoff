# Exponential backoff for handling retries with RabbitMQ
Using [Dead-Letter Queues](https://www.rabbitmq.com/dlx.html) and [Per-Queue Message TTL](https://www.rabbitmq.com/ttl.html#per-queue-message-ttl)

## Diagram
![](docs/diagram.png)

## Demo
- Start the RabbitMQ service
    ```
    docker-compose up -d
    ```
- Wait until http://localhost:15673/ is available and login with `admin/admin` for monitoring the queues
- Make sure the port `9090` is not used and start the application by running the `AmqpApplication`
- Access http://localhost:9090/numbers/enqueue/100 in browser for publishing 100 messages in the main queue (`numbers`)
- About 50% of messages will be randomly rejected from the main queue, and you can see the real time processing by accessing http://localhost:15673/#/queues
![](docs/queues.png)
