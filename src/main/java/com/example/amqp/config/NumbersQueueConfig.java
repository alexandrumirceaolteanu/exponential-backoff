package com.example.amqp.config;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
@AllArgsConstructor
public class NumbersQueueConfig {
    private static final Integer INITIAL_DELAY_SECONDS = 5;
    private static final Integer MULTIPLICATION_FACTOR = 2;

    private static final String DEFAULT_EXCHANGE = "";

    // https://www.rabbitmq.com/dlx.html#using-optional-queue-arguments
    private static final String DEAD_LETTER_EXCHANGE_ARG_KEY = "x-dead-letter-exchange";
    private static final String DEAD_LETTER_ROUTING_KEY_ARG_KEY = "x-dead-letter-routing-key";
    // https://www.rabbitmq.com/ttl.html#message-ttl-using-x-args
    private static final String MESSAGE_TTL_ARG_KEY = "x-message-ttl";

    private static final Integer SECOND = 1000;

    public static final String QUEUE_NAME = "numbers";
    public static final String PARKING_LOT_QUEUE_NAME = QUEUE_NAME + ".parkingLot";
    public static final String DEAD_LETTER_QUEUE_NAME = QUEUE_NAME + ".dlq";
    public static final Integer MAX_RETRIES = 3;

    public static String delayQueueName(Integer retryNumber) {
        return QUEUE_NAME + ".wait." + String.format("%02d", waitTime(retryNumber)) + "s";
    }

    public static Integer waitTime(Integer retryNumber) {
        if (retryNumber <= 0) {
            throw new AssertionError();
        }

        return INITIAL_DELAY_SECONDS * (int) Math.pow(MULTIPLICATION_FACTOR, retryNumber - 1);
    }

    private static Queue getDelayQueue(Integer retryNumber) {
        Map<String, Object> args = new HashMap<>();
        args.put(DEAD_LETTER_EXCHANGE_ARG_KEY, DEFAULT_EXCHANGE);
        args.put(DEAD_LETTER_ROUTING_KEY_ARG_KEY, QUEUE_NAME);
        args.put(MESSAGE_TTL_ARG_KEY, SECOND * waitTime(retryNumber));
        return new Queue(delayQueueName(retryNumber), true, false, false, args);
    }

    @Bean
    public Queue getMainQueue() {
        Map<String, Object> args = new HashMap<>();
        args.put(DEAD_LETTER_EXCHANGE_ARG_KEY, DEFAULT_EXCHANGE);
        args.put(DEAD_LETTER_ROUTING_KEY_ARG_KEY, DEAD_LETTER_QUEUE_NAME);
        return new Queue(QUEUE_NAME, true, false, false, args);
    }

    @Bean
    public Queue getDeadLetterQueue() {
        return new Queue(DEAD_LETTER_QUEUE_NAME);
    }

    @Bean
    public Queue get1stRetryQueue() {
        return getDelayQueue(1);
    }

    @Bean
    public Queue get2ndRetryQueue() {
        return getDelayQueue(2);
    }

    @Bean
    public Queue get3rdRetryQueue() {
        return getDelayQueue(3);
    }

    @Bean
    public Queue getParkingLotQueue() {
        return new Queue(PARKING_LOT_QUEUE_NAME);
    }

    @Bean
    public Jackson2JsonMessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }
}
