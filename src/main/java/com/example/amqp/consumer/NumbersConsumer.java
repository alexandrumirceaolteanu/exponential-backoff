package com.example.amqp.consumer;

import com.example.amqp.config.NumbersQueueConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@AllArgsConstructor
public class NumbersConsumer {
    private static final String X_RETRIES_HEADER = "x-retries";

    private RabbitTemplate rabbitTemplate;

    @RabbitListener(queues = NumbersQueueConfig.QUEUE_NAME)
    public void receiveEntityUpdateRequest(Integer number) throws Exception {
        if (Math.random() > 0.5) {
            throw new Exception("An unexpected error occurred");
        }
    }

    @RabbitListener(queues = NumbersQueueConfig.DEAD_LETTER_QUEUE_NAME)
    protected void rePublish(Message failedMessage) {
        Integer retriesHeader = (Integer) failedMessage.getMessageProperties().getHeaders().get(X_RETRIES_HEADER);
        if (retriesHeader == null) {
            retriesHeader = 0;
        }

        String destinationQueue;
        retriesHeader += 1;
        if (retriesHeader <= NumbersQueueConfig.MAX_RETRIES) {
            destinationQueue = NumbersQueueConfig.delayQueueName(retriesHeader);
            failedMessage.getMessageProperties().getHeaders().put(X_RETRIES_HEADER, retriesHeader);
        } else {
            destinationQueue = NumbersQueueConfig.PARKING_LOT_QUEUE_NAME;
        }

        rabbitTemplate.send(destinationQueue, failedMessage);
    }
}
