package com.example.amqp.web;

import com.example.amqp.config.NumbersQueueConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Slf4j
@Component
@AllArgsConstructor
@RequestMapping(value = "/numbers")
public class NumbersController {
    private final RabbitTemplate rabbitTemplate;

    @GetMapping("/enqueue/{total}")
    private ResponseEntity<HttpStatus> enqueueNumbers(@PathVariable Integer total) {
        int counter;
        for (counter = 1; counter <= total; counter++) {
            rabbitTemplate.convertAndSend(NumbersQueueConfig.QUEUE_NAME, counter);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
